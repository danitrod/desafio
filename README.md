# Desafio IBM | Experimente BB

[![IBM Cloud Powered](https://img.shields.io/badge/IBM%20Cloud-powered-blue.svg)](https://ibm.biz/Bdzhws)
[![Platform Node.JS](https://img.shields.io/badge/platform-nodejs-lightgrey.svg?style=flat)](https://developer.ibm.com/node/)

## Passo a passo (leia com atenção)

* [1. Sobre o Desafio](#1-sobre-o-desafio)
* [2. Pré-requisitos](#2-pré-requisitos)
* [3. Deploy do Chatbot](#3-deploy-do-chatbot)
* [4. Treinamento do Modelo Preditivo](#4-treinamento-do-modelo-preditivo)
* [5. Desenvolvimento da Action](#5-desenvolvimento-da-action)
    * [5.1. Criar instância de Natural Language Understanding](#51-criar-instância-de-natural-language-understanding)
    * [5.2. Detalhe da função e exemplo em Javascript](#52-detalhe-da-função-e-exemplo-em-javascript)
* [6. Configurar Webhook no Watson Assistant](#6-configurar-webhook-no-watson-assistant)
* [7. Deploy da página de chat](#7-deploy-da-página-de-chat)
* [8. Submissão do desafio](#8-submissão-do-desafio)

## 1. Sobre o Desafio

Neste desafio, desenvolvido pela IBM, os participantes irão testar os conhecimentos na plataforma IBM Cloud baseado no tema proposto pelo Banco do Brasil: **Computação Afetiva**. Cada participante irá seguir o guia abaixo e desenvolver na sua conta da [IBM Cloud](https://cloud.ibm.com).

Você irá aprender, pelos materiais apresentados e pelo acesso a documentação dos serviços (veja a lista abaixo), a treinar um modelo preditivo com o Modeler Flow e disponibilizar no Watson Machine Learning, além de desenvolver a sua própria função dentro do IBM Cloud Functions, plataforma Serverless baseado no Apache OpenWhisk. Tudo isso para entregar uma assistente virtual capaz de analisar emoções e analisar o melhor perfil de investimento, baseado no histórico de recomendações com o uso de Machine Learning.

Você irá utilizar os seguintes serviços:

* Watson Assistant;
* Watson Machine Learning;
* Watson Studio;
* Watson Natural Language Understanding;
* Language Translator;
* IBM Cloud Functions;
* Cloud Foundry.

## 2. Pré-requisitos

* Ter uma conta na [IBM Cloud](https://ibm.biz/Bdzhws) com o email corporativo (seu email do banco);
* Entrar na comunidade do Banco do Brasil (restrito aos funcionários).

## 3. Deploy do Chatbot

Acesse a sua conta na IBM Cloud (caso não tenha, siga o passo anterior), e acesse o [catálogo de serviços na aba AI (Inteligência Artificial)](https://cloud.ibm.com/catalog?category=ai). Clique sobre o serviço de Watson Assistant e depois clique em *Create*.

<div align="center">
    <img src="doc/source/images/Watson Assistant 01.png" alt="IBM Cloud Catalog" width="375">
    <img src="doc/source/images/Watson Assistant 02.png" alt="IBM Watson Service" width="375">
</div>

Na página de detalhes da sua instância de Watson Assistant você deve clicar no botão *Launch Watson Assistant*. Vai abrir uma nova aba com a plataforma de treinamento. Você irá importar um chatbot pronto no serviço. Clique em *Skills* e depois clique em *Create*.

<div align="center">
    <img src="doc/source/images/Watson Assistant 03.png" alt="Detail IBM Watson" width="375">
    <img src="doc/source/images/Watson Assistant 04.png" alt="Detail IBM Watson" width="375">
</div>

<div align="center">
    <img src="doc/source/images/Watson Assistant 05.png" alt="IBM Watson Platform" width="375">
    <img src="doc/source/images/Watson Assistant 06.png" alt="Import new Skill" width="375">
</div>

## 4. Treinamento do Modelo Preditivo

Neste desafio você deve treinar um modelo preditivo usando o serviço do Watson Studio e Machine Learning, com o uso da ferramenta Modeler Flow (um dos recursos dentro do Watson Studio). Caso ainda não tenha visto o vídeo distribuído no canais internos, você pode ver o vídeo abaixo.

<div align="center">
    <a href="https://youtu.be/6qXR6sEFz3Y">
        <img width="375" src="doc/source/images/Youtube-MachineLearning.png">
    </a>
</div>

## 5. Desenvolvimento da Action

Após o treinamento do seu modelo preditivo, você vai criar uma *Action*, um bloco de código, que deverá executar dois tipos de operações: analisar as emoções e analisar perfil de investimento (usando o modelo preditivo, criado no passo anterior).

Para extrair as emoções, você deverá usar o serviço de **Natural Language Understanding** (veja mais detalhes abaixo).

### 5.1. Criar instância de Natural Language Understanding

Para analisar emoções a partir das mensagens enviadas pelo usuário você deverá usar o **Natural Language Understanding** (também conhecido como **NLU**), o serviço de NLP (Natural Language Processing), que tem como uma das funcionalidades a análise de emoções. No entanto, essa funcionalidade não está disponível em Português (pt-br). Nesse caso, será necessário traduzir o texto para a língua Inglês (en-us) para retornar a lista de emoções.

Para criar a instância do NLU, você irá abrir o catálogo da [IBM Cloud, na categoria AI (ou IA)](https://cloud.ibm.com/catalog?category=ai) e localizar o serviço. Clique sobre o serviço e depois clique no botão *Create*. Você será redirecionado a tela de detalhes da sua instância.

<div align="center">
    <img src="doc/source/images/NLU 01.png" alt="IBM Cloud Catalog" width="375">
    <img src="doc/source/images/NLU 02.png" alt="IBM Watson Service" width="375">
</div>

Acesse a aba *Manage*. Salve o *API Key* que aparece na sua tela, pois você usará no desenvolvimento da sua função, ao conectar o seu código com a instância criada na sua conta. Basta clicar no ícone marcado na imagem abaixo. Ou, caso não funcione, clique sobre o botão *Show Credentials* e copie o código que irá aparecer na tela.

<div align="center">
    <img src="doc/source/images/NLU 03.png" alt="IBM Cloud Catalog" width="375">
</div>

### 5.2. Detalhe da função e exemplo em Javascript

[Clique aqui](doc/source/js/action.js) para acessar o exemplo da Action que será usada pelo Webhook no Watson Assistant. A função deve receber um parâmetro chamado "*type*" que pode ser "*ML*" ou "*Emotions*". Esse dado será usado para identificar qual serviço deverá ser chamado, ou o **Language Translator** com o **Natural Language Understanding** ou o **Machine Learning**.

A função desenvolvida receberá o seguinte JSON:

**Caso 01.** Chamada de API pelo webhook no Watson Assistant para analisar a emoção da frase.

```json
{
    "type": "Emotions",
    "text": "<texto enviado pelo usuário>"
}
```

Retorno esperado:

```json
{

}
```

**Caso 02.** Chamada de API pelo webhook no Watson Assistant para analisar o perfil de investimento (baseado no seu treinamento no Watson Studio e Watson Machine Learning).

```json
{
    "type": "ML",
    "filhos": "<números de filhos>",
    "salario": "<salário mensal>",
    "gastoMensal": "<gasto mensal>",
    "escolaridade": "<nível de escolaridade>"
}
```

Retorno esperado:

```json
{

}
```

🚨 **Você pode desenvolver a função EM QUALQUER LINGUAGEM (de programação). Não existe preferência. Desde que siga as regras descritas acima.** 🚨

## 6. Configurar Webhook no Watson Assistant

Para configurar o webhook, fizemos um vídeo para te guiar na tarefa. Assista o vídeo abaixo e veja como fazer dentro do Watson Assistant.

<div align="center">
    <a href="https://youtu.be/nqgi1Dr8tJc">
        <img width="375" src="doc/source/images/Youtube-Action.png">
    </a>
</div>

Ainda com dúvida? Acesse a documentação do Watson Assistant e leia o [guia completo](https://cloud.ibm.com/docs/services/assistant?topic=assistant-dialog-webhooks&locale=pt-br).

## 7. Deploy da página de chat

Para subir a aplicação na IBM Cloud, você deve `clicar no botão` abaixo para subir usando o IBM Continuous Delivery (também conhecido como Delivery Pipeline).

🚨 **Clique para fazer o DEPLOY da aplicação na IBM Cloud** 🚨

[![Deploy to IBM Cloud](https://cloud.ibm.com/devops/setup/deploy/button.png)](https://cloud.ibm.com/devops/setup/deploy?repository=https://gitlab.com/vshinya/desafio)

## 8. Submissão do desafio

A submissão é feita através do chatbot Katia. Após os testes e validações do seu treinamento e desenvolvimento, você deve mandar a seguinte mensagem "`Quero submeter o meu desafio`". Você irá confirmar e depois disso, o seu desafio será analisado e pontuado. Você receberá o feedback no chatbot.

🚨 **Você pode submeter quantas vezes quiser. Não há limite de submissões.** 🚨

---

## License

Copyright 2019 Maratona Behind the Code

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
